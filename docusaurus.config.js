const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: "GDM Ambientador",
  tagline: "(tagline)",
  url: "https://dymaxionlabs.gitlab.io/",
  baseUrl: "/gdm-optimus-docs/",
  onBrokenLinks: "warn",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",
  organizationName: "dymaxionlabs", // Usually your GitHub org/user name.
  projectName: "gdm-optimus-docs", // Usually your repo name.
  themeConfig: {
    navbar: {
      title: "Ambientador",
      logo: {
        alt: "Optimus Logo",
        src: "img/logo.png",
      },
      items: [
        {
          type: "doc",
          docId: "guide/intro",
          position: "left",
          label: "User Guide",
        },
        {
          href: "https://gitlab.com/dymaxionlabs/gdm-optimus-docs",
          label: "GitLab",
          position: "right",
        },
        {
          type: "localeDropdown",
          position: "right",
        },
      ],
    },
    footer: {
      style: "dark",
      links: [
        {
          title: "Docs",
          items: [
            {
              label: "User Guide",
              to: "/docs/guide/",
            },
          ],
        },
        {
          title: "Community",
          items: [
            {
              label: "Stack Overflow",
              href: "https://stackoverflow.com/questions/tagged/docusaurus",
            },
            {
              label: "Discord",
              href: "https://discordapp.com/invite/docusaurus",
            },
            {
              label: "Twitter",
              href: "https://twitter.com/docusaurus",
            },
          ],
        },
        {
          title: "More",
          items: [
            {
              label: "GitLab",
              href: "https://gitlab.com/dymaxionlabs/gdm-optimus-docs",
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Grupo Don Mario, Inc. Built with Docusaurus.`,
    },
    prism: {
      theme: lightCodeTheme,
      darkTheme: darkCodeTheme,
    },
  },
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
          editUrl:
            "https://gitlab.com/dymaxionlabs/gdm-optimus-docs/-/edit/main/",
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      },
    ],
  ],
  i18n: {
    defaultLocale: "en",
    locales: ["en", "es", "pt"],
  },
};
