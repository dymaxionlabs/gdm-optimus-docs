---
sidebar_position: 3
---

# Capas crudas

En esta sección se pueden cargar capas crudas de interés, como son mapas de
rendimiento y muestreos de suelo.

Estas capas deben ser capas vectoriales de tipo punto y los formatos admitidos
son: ESRI Shapefile, GeoJSON, KML y KMZ. En el caso de no proporcionar
información acerca de la proyección se asume que es *EPSG:4326*.

Las acciones que se pueden realizar sobre estas capas son:
* Eliminar
* Descargar (próximamente)
* Consolidar o interpolar (explicado en la sección correpondiente)

<iframe width="560" height="315" src="https://www.youtube.com/embed/2ilI4Ol2gqU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
