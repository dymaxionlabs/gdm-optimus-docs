---
sidebar_position: 6
---

# Trabajos

## Descripción de pantalla

En la sección de trabajos se muestran todos los procesos que fueron o están
siendo ejecutados. Hay 3 columnas de relevancia:

* **Tipo**: indica el tipo de proceso. Este puede ser "zoning" y "consolidation"
* **Campo**: indica el nombre del lote en cuestión.
* **Estado**: es el estado de la ejecución. Este puede ser "Running", "Successful" y "Failed"

A la izquierda de cada fila se muestran dos íconos:

* **View dashboard** (ícono de las tres barras):
  - En el caso de las consolidaciones, al hacer clic, se muestra una tabla con
    las columnas interpoladas como la que se muestra a continuación. Cada fila
    representa a una columna. A partir de la cantidad de puntos iniciales, se
    sugiere o no, incluir esa propiedad como entrada en el algoritmo para hacer
    la ambientación.

  ![](/img/jobs/view-dashboard.png)

  - En el caso de las zonificaciones o ambientaciones, al hacer clic, se muestra
    un resumen del proceso con diferentes gráficos.

* **View job run detail page on Databricks** (ícono de la lupa): tanto en el
  caso de las consolidaciones como en el caso de las ambientaciones, al hacer
  clic, se muestra un notebook de Databricks con un mayor nivel de detalle del
  proceso. Si la tarea falló por algún motivo, en este notebook se puede
  observar en qué celda falló. Es útil observar esto para reportar errores de
  una manera más eficiente.

En la parte superior de la pantalla podemos ver 3 acciones:

* **Reintentar**: en el caso de que un proceso fallara y se reportara el error, una
  vez solucionado el problema puede reintentarse correr la tarea.
* **Cancelar**: en el caso de que se desee interrumpir una tarea.
* **Clonar**: en el caso de que se desee replicar un proceso ya ejecutado con
  ligeras modificaciones, puede clonarse la tarea. Al clonar la tarea, se abrirá
  la pantalla de "consolidación" o "zonificación" según corresponda con los
  campos precargados. Al hacer las modificaciones correspondientes, se podrá
  volver a ejecutar la tarea.
<iframe width="560" height="315" src="https://www.youtube.com/embed/99TCgeR-7bM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

## Consolidación

En esta sección se realiza una interpolación a partir de capas vectoriales de tipo punto (mapas de rendimiento, muestreos de suelo) y como resultado se obtiene una grilla (vector de tipo polígono) con la resolución deseada (parámetro "Tamaño de la cuadrícula").

Para ejecutar el trabajo de consolidación hay que seleccionar las capas de interés (en la sección de capas crudas) y hacer clic en "Consolidar". Una vez abierta la nueva pantalla, hay que especificar los siguientes parámetros obligatorios:

* `Raw layers`: este parámetro hace referencia a las capas crudas de interés. 
* `Properties`: parámetro que hace referencia a las columnas de interés de las capas crudas seleccionadas. Estas columnas deben ser numéricas.
* `Grid size`: parámetro que hace referencia a la resolución de la grila. Por default el tamaño de la misma es de 10x10m.
* `Consolidated layer name`: parámetro que hace referencia al nombre de la capa consolidada.

## Zonificación

Para elegir el tipo de zonificación se debe modificar el campo "Analysis Type". Las 5 opciones son:

* Multi-layer
* Multi-year NDVI
* Multi-year NDVI Rules 4-Zones
* Multi-year NDVI Kmeans 4-Zones
* Multi-year Yield


### Multi-layer

Este tipo de ambientación recibe como entradas capas de información previamente interpoladas en la sección de "Consolidación de capas". 
Estas capas de información pueden ser de diferente índole, como por ejemplo: mapas de rendimiento, muestreos de suelo y altimetría. Adicionalmente se puede agregar información proveniente de imágenes satelitales NDVI adquiridas por Sentinel 2 (nivel de procesamiento L2A) y de elevación (SRTM).

Para ejecutar el trabajo de ambientación hay que especificar los siguientes parámetros:
* `Number of clusters`: parámetro obligatorio. Es un número entero que indica el número máximo de ambientes que se van a probar. Como salida se van a obtener diferentes zonificaciones con diferentes número de ambientes: desde 2 hasta el número indicado en este parámetro.   
* `Add NDVI`: si se marca, se incluyen imágenes NDVI al algoritmo de ambientación. Para ello hay que indicar las fechas de siembra de las campañas de interés. 
  * `Sowing dates`: parámetro obligatorio si se marca "Add NDVI". Son las fechas de siembra (de verano?) de las campañas de interés. 
  * `Filter dates`: parámetro opcional. Hace referencia a las fechas que se deseen filtrar.
* `Add Satellite Elevation`: si se marca, se incluye la información de elevación al algoritmo de ambientación.
	
* `Capa consolidada` y `properties`: parámetros obligatorios cuando `Add NDVI` y `Add Satellite Elevation` están desmarcados. 
`Capa consolidada` indica la capa donde se encuentra la información de interés interpolada generada en la sección "Consolidación de capas". `Properties` indica qué información contenida en la "Capa consolidada" incluir en el algoritmo de ambientación. Se pueden utilizar todas las propiedades o solo algunas.

* `Weigths`: parámetro no obligatorio. Si no se completa, todas las capas de información incluidas en al algoritmo de ambientación pesan lo mismo (1 por default). Si se desea darle más importancia a alguna propiedad, puede indicárselo mediante un número entero.

Como resultado, se obtienen ambientaciones con diferente cantidad de ambientes (de 2 hasta `Number of clusters`). 

<iframe width="560" height="315" src="https://www.youtube.com/embed/3wdwsWB1Wu8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Multi-year NDVI

Este tipo de ambientación recibe como entradas capas de información proveniente de imágenes satelitales NDVI adquiridas por Sentinel 2 (nivel de procesamiento L2A). 

Para ejecutar el trabajo de ambientación hay que especificar los siguientes parámetros:
* `Sowing dates`: parámetro obligatorio. Son las fechas de siembra de las campañas de interés. 
* `Filter dates (NDVI)`: parámetro no obligatorio. Hace referencia a las fechas que se desean excluir del análisis.

Como resultado, se obtiene una ambientación con 6 ambientes. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/RVdS5GS_LME" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Multi-year NDVI rules 4-zones

Este tipo de ambientación recibe como entradas capas de información proveniente de imágenes satelitales NDVI adquiridas por Sentinel 2 (nivel de procesamiento L2A). 

Para ejecutar el trabajo de ambientación hay que especificar los siguientes parámetros:

* `Sowing dates`: parámetro obligatorio. Son las fechas de siembra de las campañas de interés. 
* `Filter dates (NDVI)`: parámetro no obligatorio. Hace referencia a las fechas que se desean excluir del análisis.
* `Variability threshold`: parámetro obligatorio (default 30) que indica el umbral a partir del cual un ambiente se considera de productividad intestable.
* `Productivity threshold`: parámetro obligatorio (default 80) que indica el umbral a partir del cual un ambiente se considera de alta productividad. 

Como resultado, se obtiene una ambientación con 4 ambientes. 
	
### Multi-year NDVI K-means 4-zones

Este tipo de ambientación recibe como entradas capas de información proveniente de imágenes satelitales NDVI adquiridas por Sentinel 2 (nivel de procesamiento L2A). 

Para ejecutar el trabajo de ambientación hay que especificar los siguientes parámetros:

* `Sowing dates`: parámetro obligatorio. Son las fechas de siembra de las campañas de interés. 
* `Filter dates` (NDVI): parámetro no obligatorio. Hace referencia a las fechas que se desean excluir del análisis.

Como resultado, se obtiene una ambientación con 4 ambientes. 

### Multi-year Yield

Este tipo de ambientación recibe como entradas una capa consolidada con mapas de rendimiento interpolados de más de una campaña. Esta capa consolidada es generada en la sección "Consolidación de capas".

Para ejecutar el trabajo de ambientación hay que especificar **obligatoriamente** los siguientes parámetros:

* `Capa consolidada`:  indica la capa consolidada donde se encuentran los mapas de rendimiento de interés interpolados generada en la sección "Consolidación de capas".
* `Properties`:  indica qué información contenida en la "Capa consolidada" incluir en el algoritmo de ambientación. Se pueden utilizar todos los mapas de rendimiento o solo algunos. 

Como resultado, se obtiene una ambientación con 6 ambientes. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/H2ZM2er4zQc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
