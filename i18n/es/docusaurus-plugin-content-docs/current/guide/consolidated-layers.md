---
sidebar_position: 4
---

# Capas consolidadas

## Nueva consolidación

En esta sección se realiza una interpolación a partir de capas vectoriales de tipo punto (mapas de
rendimiento, muestreos de suelo) y como resultado se obtiene una grilla (vector de tipo polígono) con la
resolución deseada.

Para ejecutar el trabajo de consolidación hay que seleccionar las capas de interés (en la sección de capas
crudas) y hacer clic en "Consolidar". Una vez abierta la nueva pantalla, hay que especificar los siguientes
parámetros obligatorios:

### Parámetros

* Raw layers: Este parámetro hace referencia a las capas crudas de interés. Se
  admiten diferentes proyecciones.
* Properties: Parámetro que hace referencia a las columnas de interés de las
  capas crudas seleccionadas. Estas columnas deben ser numéricas.
* Grid size: Parámetro que hace referencia a la resolución de la grila. Por
  default el tamaño de la misma es de 10x10m.
* Consolidated layer name: Parámetro que hace referencia al nombre de la capa
  consolidada.

## Listado de capas

Las acciones que se pueden realizar sobre estas capas son:

* Eliminar
* Descargar
* Ver trabajo: en esta sección se pueden ver los parámetros de entrada que se
  utilizaron para ejecutar la tarea de consolidación.
* Zonificar (explicado en la sección correspondiente)

<iframe width="560" height="315" src="https://www.youtube.com/embed/LuSWRzxVt2A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Pp9bMqAGlto" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
