---
sidebar_position: 2
---

# Lote

## Crear un lote 

En esta sección de carga el perímetro del lote de interés.
Damian Gomez..
El tipo de capa es vectorial de tipo polígono y los formatos admitidos son: ESRI
Shapefile, GeoJSON y KML. En el caso de no proporcionar información acerca
de la proyección se asume que es *EPSG:4326*.

En el caso de utilizar el formato shapefile, se deben cargar todos los archivos
correspondientes **sin comprimir**.

Se debe indicar:
* Nombre del lote
* Nombre del campo
* Nombre del productor

Una vez seleccionados el/los archivos se debe hacer clic en "Crear".

Una vez creado el lote, en la parte superior aparecen 3 secciones:

* Capas crudas: aquí se cargan las capas crudas (mapas de rendimiento, muestreos
  de suelo, etc.).
* Capas consolidas: luego de hacer una consolidación, las capas consolidas van a
  aparecer aquí.
* Capas de zona: luego de hacer una ambientación, las zonificaciones van a
  aparecer aquí.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Vz723vpuN0c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Compartir un lote

Si se desea compartir un lote, se debe ingresar al mismo y presionar el botón "Compartir". 

Hay dos modos de compartir:

* Modo lector
* Modo editor

Una vez compartido el lote, el mismo queda visible o editable, según corresponda, para el usuario con el que se compartió (capas crudas, capas consolidadas, zonificaciones y trabajos).

![](/img/compartir_lote_1.png)

## Eliminar un lote

Si se desea eliminar un lote, se debe ingresar al mismo y presionar el botón "Eliminar". 

![](/img/eliminar_lote.png)
