---
sidebar_position: 5
---

# Capas de ambientes

Las acciones que se pueden realizar sobre estas capas son:

* Eliminar
* Descargar
* Ver trabajo: en esta sección se pueden ver los parámetros de entrada que se utilizaron para ejecutar la tarea de ambientación.
* Caracterizar: se realiza un análisis cuantitativo de los valores promedio y dispersión de las variables utilizadas para realizar la ambientación. Adicionalmente se describen las diferencias entre zonas. Más detalle en la sección de trabajos.
* Recomendar: se puede añadir a la capa de zonas la recomendación de las variables de manejo que se deseen (densidad de siembra, fertilización, etc.)
