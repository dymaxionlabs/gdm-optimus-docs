---
sidebar_position: 3
---

# Camadas brutas

Nesta seção, você pode carregar camadas brutas de interesse, como mapas de
rendimento e amostragem de solo.

Essas camadas devem ser camadas vetoriais do tipo ponto e os formatos suportados
São eles: ESRI Shapefile, GeoJSON, KML e KMZ. No caso de não fornecer
As informações sobre a projeção são assumidas como *EPSG:4326*.

As ações que podem ser realizadas nessas camadas são:
* Livrar-se de
* Download (em breve)
* Consolidar ou interpolar (explicado na seção correspondente)

<iframe width="560" height="315" src="https://www.youtube.com/embed/2ilI4Ol2gqU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
