---
sidebar_position: 4
---

# Camadas consolidadas

## Nova consolidação

Nesta seção, uma interpolação é realizada a partir de camadas vetoriais do tipo
ponto (mapas de rendimento, amostragens de solo) e como resultado uma grade é
obtida (vetor do tipo polígono) com o resolução desejada.

Para executar o trabalho de consolidação, selecione as camadas de interesse (na
seção de camadas raw) e clique em "Consolidar". Assim que a nova tela for
aberta, você deve especificar o seguinte parâmetros obrigatórios:

### Parâmetros

* Camadas brutas: Este parâmetro se refere às camadas brutas de interesse. eu
  sei eles admitem diferentes projeções.
* Propriedades: Parâmetro que se refere às colunas de interesse do camadas
  brutas selecionadas. Essas colunas devem ser numéricas.
* Tamanho da grade: Parâmetro que se refere à resolução da grade. Para o tamanho
  padrão é 10x10m.
* Nome da camada consolidada: parâmetro que se refere ao nome da camada
  consolidado.

## Lista de camadas

As ações que podem ser realizadas nessas camadas são:

* Livrar-se de
* Download
* Ver trabalho: nesta seção você pode ver os parâmetros de entrada que são usado
  para executar a tarefa de consolidação.
* Zoneamento (explicado na seção correspondente)

<iframe width="560" height="315" src="https://www.youtube.com/embed/LuSWRzxVt2A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Pp9bMqAGlto" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
