---
sidebar_position: 2
---

# Lote

## Criar lote 

Nesta seção carregue o perímetro do lote de interesse.

O tipo de camada é um vetor do tipo polígono e os formatos suportados são: ESRI
Shapefile, GeoJSON e KML. No caso de não fornecer informações sobre a
projeção é assumida como * EPSG: 4326 *.

No caso de usar o formato shapefile, todos os arquivos devem ser carregados
correspondente ** não compactado **.

Deve ser indicado:
* Nome do lote
* Nome do campo
* Nome do produtor

Depois de selecionar os arquivos, clique em "Criar".

Depois que o lote é criado, três seções aparecem na parte superior:

* Camadas brutas: as camadas brutas são carregadas aqui (mapas de rendimento,
   amostragens solo, etc.).
* Camadas consolidadas: após fazer uma consolidação, as camadas consolidadas
   irão apareça aqui.
* Camadas de zona: após fazer uma configuração, o zoneamento irá apareça aqui.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Vz723vpuN0c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Compartilhar lote

Para compartilhar um lote, clicar no botão "Compartilhar".

Existem duas maneiras de compartilhar:

* Modo Leitor
* Modo Editor

![](/img/compartir_lote_1.png)

## Apagar lote

Para apagar um lote, clicar no botão "Excluir". 

![](/img/eliminar_lote.png)
