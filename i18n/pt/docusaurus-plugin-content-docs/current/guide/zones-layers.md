---
sidebar_position: 5
---

# Camadas de ambientes

## Nova configuração

Para escolher o tipo de zoneamento, deve-se modificar o campo "Tipo de análise".
As 3 opções são:

* Multicamada
* NDVI plurianual
* Rendimento de vários anos

## Métodos de aromatização

### Multicamada

Este tipo de configuração recebe previamente camadas de informações como
entradas. interpolado na seção "Consolidação de camadas".

Essas camadas de informações podem ser de diferentes naturezas, como: mapas de
produção, amostragem de solo e altimetria. Além disso, você pode adicionar
informações de imagens de satélite NDVI adquiridas por Sentinela 2 (nível de
processamento L2A) e elevação (SRTM).

Para executar o trabalho de aroma, você deve especificar o seguinte parâmetros:

#### Parâmetros

* Número de clusters: Parâmetro obrigatório. É um número inteiro que indica o
  número máximo de ambientes a serem testados. Como sair zoneamento diferente
  com diferentes números de quartos: de 2 a o número indicado neste parâmetro.

* Adicionar NDVI: Se marcado, as imagens NDVI são incluídas no algoritmo de
  ambiente. Para isso, é necessário indicar as datas de semeadura das campanhas
  de interesse.
  - Datas de semeadura: parâmetro obrigatório se "Adicionar NDVI" estiver
    marcado. São as datas semeadura (verão?) das campanhas de interesse.
  - Filtrar datas: parâmetro opcional. Refere-se às datas desejadas filtro.
    Observações: para deletar uma data basta refazer clique na data que deseja
    editar.

* Adicionar Elevação de Satélite: Se marcado, as informações de elevação são
  incluídas para o algoritmo de aromatização.

* Camada e propriedades consolidadas: Parâmetros obrigatórios.
  - "Camada consolidada" indica a camada onde as informações do Interpolados de
    interesse gerados na seção "Consolidação de camadas".
  - "Propriedades" indica quais informações estão contidas na "Camada
    Consolidada" incluir no algoritmo de aromatização. Você pode usar todos os
    propriedades ou apenas alguns.
  - "Pesos": é um parâmetro não obrigatório. Se não for concluído, todos As
    camadas de informações incluídas no algoritmo de configuração pesam tanto
    quanto mesmo (1 por padrão). Se você quiser dar mais importância a uma
    propriedade, pode ser indicado por um número inteiro.


<iframe width="560" height="315" src="https://www.youtube.com/embed/3wdwsWB1Wu8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Multiyear NDVI

Este tipo de configuração recebe como entradas camadas de informações de de
imagens de satélite NDVI adquiridas pelo Sentinel 2 (nível de processamento
L2A). Para executar o trabalho de aroma, você deve especificar o seguintes
parâmetros:

#### Parâmetros

* Adicionar NDVI: Este parâmetro deve ser marcado como obrigatório.
  - Datas de semeadura: parâmetro obrigatório. São as datas de semeadura
    (verão?) das campanhas de interesse.
  - Datas do filtro: parâmetro não obrigatório. Refere-se a ...

<iframe width="560" height="315" src="https://www.youtube.com/embed/RVdS5GS_LME" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Multiyear yield

Este tipo de configuração recebe como entradas uma camada consolidada com mapas
de desempenho interpolado de mais de uma campanha. Esta camada consolidada é
gerado na seção "Consolidação de camadas".

Para executar o trabalho de aroma, você deve especificar o seguinte parâmetros:

#### Parâmetros

* Camada e propriedades consolidadas: Parâmetros obrigatórios.
  - "Camada consolidada" indica a camada consolidada onde os mapas estão
    localizados dos rendimentos de juros interpolados gerados na seção
    "Consolidação de camadas ".
  - "Propriedades" indica quais informações estão contidas na "Camada
    Consolidada" incluir no algoritmo de aromatização. Todos os mapas podem ser
    usados desempenho ou apenas alguns.
  - Pesos: não preencha


<iframe width="560" height="315" src="https://www.youtube.com/embed/H2ZM2er4zQc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Lista de camadas

As ações que podem ser realizadas nessas camadas são:

* Livrar-se de
* Download
* Ver trabalho: nesta seção você pode ver os parâmetros de entrada que são usado
  para executar a tarefa de perfume.
