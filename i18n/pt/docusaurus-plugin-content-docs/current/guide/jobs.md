---
sidebar_position: 6
---

# Execuções de trabalho

A seção de trabalhos mostra todos os processos que foram ou são sendo executado.
Existem 3 colunas de relevância:

* **Tipo**: indica o tipo de processo. Isso pode ser "zoneamento" e
  "consolidação"
* **Campo**: indica o nome do lote em questão.
* **Status**: é o status da execução. Isso pode ser "Executando", "Bem-sucedido"
  e "Fracassado"

Dois ícones são exibidos à esquerda de cada linha:

* **Ver painel** (ícone das três barras):
  - No caso de consolidações, quando você clica, uma tabela é exibida com
    colunas interpoladas como a mostrada abaixo. Cada fila representa uma
    coluna. A partir do número de pontos iniciais, sugere ou não, inclua essa
    propriedade como entrada no algoritmo para fazer a ambientação.

  ![](/img/jobs/view-dashboard.png)

  - No caso de zoneamento ou configurações, ao clicar, mostra um resumo do
    processo com gráficos diferentes.

* **Visualize a página de detalhes da execução do trabalho no Databricks**
  (ícone da lupa): ambos no no caso de consolidações como no caso de
  configurações, fazendo clique, um bloco de notas Databricks é exibido com um
  nível mais alto de detalhes do processar. Se a tarefa falhou por qualquer
  motivo, neste notebook você pode observe em qual célula ele falhou. É útil
  observar isso para relatar erros de uma forma mais eficiente.

No topo da tela, podemos ver 3 ações:

* **Tentar novamente**: caso um processo falhe e o erro seja relatado, um Assim
  que o problema for resolvido, pode ser tentado novamente para executar a
  tarefa.
* **Cancelar**: caso você queira interromper uma tarefa.
* **Clonar**: caso você queira replicar um processo já executado com pequenas
  modificações, a tarefa pode ser clonada. A clonagem da tarefa abrirá a tela de
  "consolidação" ou "zoneamento" conforme apropriado para o campos
  pré-carregados. Fazendo as modificações correspondentes, você pode execute
  novamente a tarefa.

<iframe width="560" height="315" src="https://www.youtube.com/embed/99TCgeR-7bM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
