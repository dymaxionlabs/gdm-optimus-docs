---
sidebar_position: 3
---

# Raw layers

In this section you can load raw layers of interest, such as maps of yield and
soil sampling.

These layers must be vector layers of type point and the supported formats They
are: ESRI Shapefile, GeoJSON, KML and KMZ. In the case of not providing
Information about the projection is assumed to be *EPSG: 4326*.

The actions that can be carried out on these layers are:
* Delete
* Download (coming soon)
* Consolidate (explained in the corresponding section)

<iframe width="560" height="315" src="https://www.youtube.com/embed/2ilI4Ol2gqU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
