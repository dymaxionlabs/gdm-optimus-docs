---
sidebar_position: 5
---

# Zones layers

## New zonification

To choose the type of zoning, the "Analysis Type" field must be modified. The 3
options are:

* Multi-layer
* Multi-year NDVI
* Multi-year Yield

## Analysis methods

### Multi-layer

This type of setting receives previously layers of information as inputs.
interpolated in the "Layer Consolidation" section.

These layers of information can be of different nature, such as: yield maps,
soil sampling and altimetry. Additionally you can add information from NDVI
satellite images acquired by Sentinel 2 (L2A processing level) and elevation
(SRTM).

To run the scent job, you must specify the following parameters:

#### Parameters

* Number of clusters: Mandatory parameter. It is an integer that indicates the
  maximum number of environments to be tested. How to get out different zoning
  with different numbers of rooms: from 2 to the number indicated in this
  parameter.

* Add NDVI: If checked, NDVI images are included in the ambience algorithm. To
  do this, you must indicate the sowing dates of the campaigns of interest.
  - Sowing dates: mandatory parameter if "Add NDVI" is checked. They are the
    sowing dates (from summer?) of the campaigns of interest.
  - Filter dates: optional parameter. Refers to the desired dates filter.
    Observations: to delete a date, you just have to redo click on the date you
    want to edit.

* Add Satellite Elevation: If checked, elevation information is included to the
  flavoring algorithm.

* Consolidated layer and properties: Mandatory parameters.
  - "Consolidated layer" indicates the layer where the interpolated information
    of interest is located generated in the section "Consolidation of layers".
  - "Properties" indicates what information contained in the "Consolidated
    Layer" include in the flavoring algorithm. You can use all properties or
    just some.
  - "Weights": It is a non-mandatory parameter. If not completed, all The layers
    of information included in the scent algorithm weigh as much as same (1 by
    default). If you want to give more importance to a property, it can be
    indicated by an integer.

<iframe width="560" height="315" src="https://www.youtube.com/embed/3wdwsWB1Wu8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Multi-year NDVI

This type of setting receives layers of information from images as inputs. NDVI
satellites acquired by Sentinel 2 (L2A processing level). To run the scent job,
you must specify the following parameters:

#### Parameters

* Add NDVI: This parameter must be mandatory.
  - Sowing dates: mandatory parameter. Are the sowing dates (summer?) of the
    campaigns of interest.
  - Filter dates: not mandatory parameter. Make reference to ...

<iframe width="560" height="315" src="https://www.youtube.com/embed/RVdS5GS_LME" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Multi-year Yield

This type of setting receives as inputs a consolidated layer with maps of
interpolated performance of more than one campaign. This consolidated layer is
generated in the section "Consolidation of layers".

To run the scent job, you must specify the following parameters:

#### Parameters

* Consolidated layer and properties: Mandatory parameters.
  - "Consolidated layer" indicates the consolidated layer where the maps are
    located of interpolated interest yields generated in the section
    "Consolidation of layers ".
  - "Properties" indicates what information contained in the "Consolidated
    Layer" include in the flavoring algorithm. All maps can be used performance
    or just a few.
  - Weigths: Do not complete

<iframe width="560" height="315" src="https://www.youtube.com/embed/H2ZM2er4zQc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Layers list

The actions that can be performed on these layers are:

* Delete
* Download
* View Job: in this section you can see the input parameters that are used to
  run the scent task.
* Characterize
* Recommend
