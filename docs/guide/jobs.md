---
sidebar_position: 6
---

# Jobs

The jobs section shows all the processes that were or are being executed. There
are 3 columns of relevance:

* **Type**: indicates the type of process. This can be "zoning" and
  "consolidation"
* **Field**: indicates the name of the batch in question.
* **Status**: is the status of the execution. This can be "Running", "Succesful"
  and "Failed"

Two icons are displayed to the left of each row:

* **View dashboard** (icon of the three bars):
  - In the case of consolidations, when you click, a table is displayed with
    interpolated columns like the one shown below. Each row represents a column.
    From the number of initial points, suggests or not, include that property as
    input in the algorithm to do the ambientation.

  ![](/img/jobs/view-dashboard.png)

  - In the case of zoning or settings, when you click, it shows a summary of the
    process with different graphics.

* **View job run detail page on Databricks** (magnifying glass icon): both in
  the in the case of consolidations as in the case of settings, by making click,
  a Databricks notebook is displayed with a higher level of detail of the
  process. If the task failed for any reason, on this notebook you can observe
  in which cell it failed. It is useful to observe this for reporting errors of
  a more efficient way.

At the top of the screen we can see 3 actions:

* **Retry**: in the event that a process fails and the error is reported, a Once
  the problem has been solved, it can be retried to run the task.
* **Cancel**: in case you want to interrupt a task.
* **Clone**: in case you want to replicate a process already executed with
  slight modifications, the task can be cloned. Cloning the task will open the
  "consolidation" or "zoning" screen as appropriate to the preloaded fields. By
  making the corresponding modifications, you can rerun the task.

<iframe width="560" height="315" src="https://www.youtube.com/embed/99TCgeR-7bM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
