---
sidebar_position: 4
---

# Consolidated layers

## New consolidation

In this section, an interpolation is carried out from vector layers of the point
type (maps of yield, soil samplings) and as a result a grid is obtained (polygon
type vector) with the desired resolution.

To execute the consolidation job, select the layers of interest (in the layers
section raw) and click on "Consolidate". Once the new screen is open, you must
specify the following mandatory parameters:

### Parameters

* **Raw layers**: This parameter refers to the raw layers of interest. I know
  they admit different projections.
* **Properties**: Parameter that refers to the columns of interest of the
  selected raw layers. These columns must be numeric.
* **Grid size**: Parameter that refers to the resolution of the grid. For
  default size is 10x10m.
* **Consolidated layer name**: Parameter that refers to the name of the layer
  consolidated.

## Layers list

The actions that can be performed on these layers are:

* Delete
* Download
* View job: in this section you can see the input parameters that are used to
  run the consolidation task.
* Zone (explained in the corresponding section)

<iframe width="560" height="315" src="https://www.youtube.com/embed/LuSWRzxVt2A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Pp9bMqAGlto" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
