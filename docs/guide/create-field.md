---
sidebar_position: 2
---

# Field

## Create field

In this section load the perimeter of the field of interest.

The type of layer is vector of polygon type and the supported formats are: ESRI
Shapefile, GeoJSON and KML. In the case of not providing information about
the projection is assumed to be *EPSG:4326*.

In the case of using the shapefile format, all files must be loaded
corresponding **uncompressed**.

It should be indicated:
* Field name
* Farm Name
* Grower name

Once the files have been selected, click on "Create".

Once the field is created, 3 sections appear at the top:

* **Raw layers**: raw layers are loaded here (yield maps, samplings soil, etc.).
* **Consolidated layers**: after doing a consolidation, the consolidated layers
   will appear here.
* **Zone layers**: after making a setting, the zoning will appear here.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Vz723vpuN0c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Share field
If you want to share a field with another user, press the "Share" button.

There are two ways to share a field:
* Viewer mode
* Editor mode

![](/img/compartir_lote_1.png)

## Delete field

If you want to delete a field, press the "Delete" button.

![](/img/eliminar_lote.png)
